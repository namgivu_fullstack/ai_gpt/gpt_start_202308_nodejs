/* ref .../openai-quickstart-node/pages/api/generate.js */

import dotenv from 'dotenv'
//
import {OpenAIApi, Configuration} from 'openai'

//region load OPENAI_API_KEY
let SH=process.env.PWD  // process.env.PWD <- dodge __dirname error > ReferenceError: __dirname is not defined in ES module scope  ref https://stackoverflow.com/a/28279609/248616
let AH=`${SH}/..`
dotenv.config({ path:`${AH}/.env` })
//    .config   path <- custom .env path ref https://stackoverflow.com/a/69694614/248616

let apiKey = process.env.OPENAI_API_KEY
//endregion load OPENAI_API_KEY

let config = new Configuration({apiKey})
let openai = new OpenAIApi(config)

let temperature = .6  // 0 same prompt same result, 1 same prompt diff result, 0-1 in the middle  ref https://platform.openai.com/docs/quickstart/adjust-your-settings
let model       = 'text-davinci-003'  //TODO why choose this in the quickstart?  //TODO what cheapest model to use/try?

let prepare_prompt = (animal_name) => {
  return `Suggest two names for an animal
  
Animal: cat
Names:  Cat1, Cat2
Animal: dog
Names:  Dog1, Dog2, Dog3 
Animal: ${animal_name}
Names:`

}
let prompt = prepare_prompt('dog')

let completion = await openai.createCompletion({model, prompt, temperature})
console.log(completion)
console.log(completion.data.choices[0].text)
