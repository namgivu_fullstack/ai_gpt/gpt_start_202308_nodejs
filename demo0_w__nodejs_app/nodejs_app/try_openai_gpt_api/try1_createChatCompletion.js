/* ref .../openai-quickstart-node/pages/api/generate.js */

import dotenv from 'dotenv'
//
import {OpenAIApi, Configuration} from 'openai'

//region load OPENAI_API_KEY
let SH=process.env.PWD  // process.env.PWD <- dodge __dirname error > ReferenceError: __dirname is not defined in ES module scope  ref https://stackoverflow.com/a/28279609/248616
let AH=`${SH}/..`
dotenv.config({ path:`${AH}/.env` })
//    .config   path <- custom .env path ref https://stackoverflow.com/a/69694614/248616

let apiKey = process.env.OPENAI_API_KEY
//endregion load OPENAI_API_KEY

   let config = new Configuration({apiKey})
// let config = new Configuration({apiKey, organization: 'org-q9qoay2eTSTwWqlvspCt1gMs'})  // tried openai acc. that been invited to this org
   let openai = new OpenAIApi(config)

let model  = 'gpt-3.5-turbo'  //must use this model, cannot use davinci-xx as calling createCompletion()
let prompt = [
  {role:'system', content:'hi'},
]

let completion = await openai.createChatCompletion({model, messages:prompt })
console.log(completion)
console.log(completion.data.choices[0].message)
