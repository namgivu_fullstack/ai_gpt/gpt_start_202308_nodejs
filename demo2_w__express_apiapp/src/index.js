let express = require('express')
let assert = require('assert')
//
const {generate} = require('./service/openai_gpt')


let app = express()
app.use(express.json())  // parse reqbody fr 'json str' to 'js json obj'


///region route def

//region /ping
let on_ping=(req,res)=>{ res.json({}) }

app.get('/',            on_ping)
app.get('/ping',        on_ping)
app.get('/healthcheck', on_ping)
//endregion /ping


//region POST /generate
POST_generate = async (req,res,next)=>{
  try {
    // load+validate param
    let {prompt} = req.body ; assert(prompt!==undefined, 'reqbody field is required :prompt' )

    let r = await generate(prompt)
    console.log(`POST /generate\n`, r)
    res.json(r)
  } catch (e) {
    next(e)
    // console.log('TODO print err wisely')
  }
}

app.post('/api/generate', POST_generate)
//endregion POST /generate

///endregion route def


//region custom error handler
app.get('/api/test__custom_err_handler', (req, res, next) => {
  try {
    throw Error('ERROR test__custom_err_handler')
  } catch (e) { next(e) }
})

/* ref. https://reflectoring.io/express-error-handling/ */
let custom_err_handler = (err, req, res, next) => {
  // cook :errmsg
  let errmsg = `--- ERROR\n${err.message}\n\n--- STACK TRACE\n${err.stack}`

  // show err in console
  console.log(errmsg)

  // show err in :res
  let        res_status = err.status || 500
  res.status(res_status).send(errmsg)
}

app.use(custom_err_handler)
//CAUTION must define error-handling here AS LAST middleware ref. https://expressjs.com/en/guide/error-handling.html#writing-error-handlers

//endregion custom error handler


//   =process.env.PORT allow custom running port otherthan 3000 w/ envvar PORT
let p=process.env.PORT||3000; app.listen(p, ()=>{ console.log(`Listening ${p} ...`) })
//   =                      ;           (   ()=>see api start  Listening :port...
