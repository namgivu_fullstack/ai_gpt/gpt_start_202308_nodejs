let dotenv = require('dotenv')
let fs = require('fs')
let assert = require('assert')
//
let {OpenAIApi, Configuration} = require('openai')


//region load OPENAI_API_KEY
dotenv.config({ path:`${__dirname}/.env` })  //NOTE process.env.PWD not working here ie it's the pwd of where script start the api run and NOT this containing dir
//    .config   path <- custom .env path ref https://stackoverflow.com/a/69694614/248616

let apiKey = process.env.OPENAI_API_KEY ; assert(apiKey, 'envvar OPENAI_API_KEY is required')
//endregion load OPENAI_API_KEY

let config = new Configuration({apiKey})
let openai = new OpenAIApi(config)

async function generate(prompt) {
  let model = 'gpt-3.5-turbo'  //model gpt-3.5-turbo / gpt4 MUST duo use w/ openai.createChatCompletion()

  let messages = [
    {role: 'system', 'content': prompt},
  ]

  let completion = await openai.createChatCompletion({model, messages})
  let gpt_result = completion.data.choices[0].message

  let    r = {prompt, gpt_result, gpt_model:model}
  return r
}

// exports.default = generate
// export default generate
module.exports = {generate}

// let r = generate() ; console.log(r) // turnon for debug
