--- run apiapp
./zscript/r1_npm_i_start.sh

--- call endpoint 
== call healthcheck
http :3000/

== call /generate
curl -H 'Content-Type: application/json' \
  -X POST \
  -d '{"prompt": "hi"}' \
  localhost:3000/api/generate

consoleoutput
```
Listening 3000 ...
POST /generate
{
prompt: [ { role: 'system', content: 'hi' } ],
gpt_result: { role: 'assistant', content: 'Hello! How can I help you today?' },
gpt_model: 'gpt-3.5-turbo'
}
```

== 2nd sample call
curl -H 'Content-Type: application/json'   -X POST   -d '{"prompt": "who are you"}'   localhost:3000/api/generate
{"prompt":"who are you","gpt_result":{"role":"assistant","content":"I am an AI designed to assist with various tasks and provide information."},"gpt_model":"gpt-3.5-turbo"}
