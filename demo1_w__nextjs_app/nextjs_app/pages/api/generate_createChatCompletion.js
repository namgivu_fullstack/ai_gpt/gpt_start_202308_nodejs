import { Configuration, OpenAIApi } from 'openai'

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
})
const openai = new OpenAIApi(configuration)

export default async function (req, res) {
  try {
    // const prompt = req.body.prompt  //TODO: 'will pass prompt param after succeed calling openai api w/ apikey'
    const completion = await openai.createChatCompletion({
      model  : 'gpt-3-turbo',
      prompt : 'hi',
    })
    res.status(200).json({ result: completion.data.choices[0].text })

  } catch(error) {
    console.error(`${error}`)  // code to see full error in running console  > console.error(error)
    res.status(500).json({error})  // print error detail so that can see it in Postman
  }
}
