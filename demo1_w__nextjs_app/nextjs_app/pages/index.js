import Head from 'next/head'
import { useState } from 'react'
//
import styles from './index.module.css'

export default function Home() {
  const [result1, setResult1] = useState()
  const [result2, setResult2] = useState()

  async function onSubmit_createChatCompletion(event) {
    event.preventDefault()

    try {
      const res = await fetch('/api/generate_createChatCompletion', {
        method  : 'POST',
        headers : {'Content-Type': 'application/json'},
        body    : JSON.stringify({ TODO: 'will pass prompt param after succeed calling openai api w/ apikey' }),
      })
      const data = await res.json()
      setResult1(data.result)

    } catch(e) {
      // Consider implementing your own error handling logic here
      console.error(e)
      if (e.message) { setResult1(e.message) }  // update :state w/ setXX(e) will crash ie require e.message not e
    }
  }

  async function onSubmit_createCompletion(event) {
    event.preventDefault()

    try {
      const res = await fetch('/api/generate_createCompletion', {
        method  : 'POST',
        headers : {'Content-Type': 'application/json'},
        body    : JSON.stringify({ TODO: 'will pass prompt param after succeed calling openai api w/ apikey' }),
      })
      const data = await res.json()
      setResult2(data.result)

    } catch(e) {
      // Consider implementing your own error handling logic here
      console.error(e)
      if (e.message) { setResult2(e.message) }  // update :state w/ setXX(e) will crash ie require e.message not e
    }
  }

  return (
    <>
      <Head>
        <title>OpenAI Quickstart ref</title>
      </Head>

      <main className={styles.main}>
        <h2>NOTE this results 401, why?</h2>
        <form onSubmit={onSubmit_createChatCompletion}>
          <input type='submit' value='Run openai.createChatCompletion()' />
        </form>
        <div className={styles.result}>result: {result1}</div>

        <hr/>

        <h2>NOTE this results 200 while the above fails </h2>
        <form onSubmit={onSubmit_createCompletion}>
          <input type='submit' value='Run openai.createCompletion()' />
        </form>

        <div className={styles.result}>result: {result2}</div>
      </main>
    </>
  )
}
